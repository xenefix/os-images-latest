#!/bin/bash
set -x
DATE=`date +%Y%m%d`
DATE='latest'
IMAGES_DIR='/var/lib/libvirt/images'
CLOUDIMAGE_FEDORA29='https://download.fedoraproject.org/pub/fedora/linux/releases/29/Cloud/x86_64/images/Fedora-Cloud-Base-29-1.2.x86_64.qcow2'
CLOUDIMAGE_UBUNTU1804='https://cloud-images.ubuntu.com/bionic/current/bionic-server-cloudimg-amd64.img'
CLOUDIMAGE_CENTOS7='https://cloud.centos.org/centos/7/images/CentOS-7-x86_64-GenericCloud.qcow2c'
CLOUDIMAGE_DEBIAN='http://cdimage.debian.org/cdimage/openstack/current-9/debian-9-openstack-amd64.qcow2'
# CLOUDIMAGE_RHEL7='redhatdoesntlikemedistributingtheirimagessadface'
# CLOUDIMAGE_RHEL8='redhatdoesntlikemedistributingtheirimagessadface'

function ubuntu1804 {
  cd $IMAGES_DIR
  curl -4 -Lo $IMAGES_DIR/ubuntu1804-$DATE $CLOUDIMAGE_UBUNTU1804
  virt-customize -v \
    -a $IMAGES_DIR/ubuntu1804-$DATE \
    --update \
    --install nano \
    --run-command "echo 'datasource:\n OpenStack:\n  apply_network_config: True' | tee /etc/cloud/cloud.cfg.d/98-openstack-network.cfg"
  qemu-img convert -O qcow2 \
    -c $IMAGES_DIR/ubuntu1804-$DATE \
    $IMAGES_DIR/ubuntu1804-$DATE.qcow2
  rm -rf $IMAGES_DIR/ubuntu1804-$DATE
  curl --header "AccessKey: $BCDN_ACCESSKEY" --upload-file \
    $IMAGES_DIR/ubuntu1804-$DATE.qcow2 \
    https://storage.bunnycdn.com/blob/ubuntu1804-$DATE.qcow2
}

function debian9 {
  curl -Lo $IMAGES_DIR/debian9-$DATE $CLOUDIMAGE_DEBIAN
  virt-customize -v \
    -a $IMAGES_DIR/debian9-$DATE \
    --update \
    --install nano
  qemu-img convert -O qcow2 \
    -c $IMAGES_DIR/debian9-$DATE \
    $IMAGES_DIR/debian9-$DATE.qcow2
  rm -rf $IMAGES_DIR/debian9-$DATE
  curl --header "AccessKey: $BCDN_ACCESSKEY" --upload-file \
    $IMAGES_DIR/debian9-$DATE.qcow2 \
    https://storage.bunnycdn.com/blob/debian9-$DATE.qcow2

}

function centos7 {
  curl -Lo $IMAGES_DIR/centos7-$DATE $CLOUDIMAGE_CENTOS7
  virt-customize -v \
    -a $IMAGES_DIR/centos7-$DATE \
    --run-command "yum -y update" \
    --run-command "yum -y install nano" \
    --run-command "yum clean all" \
    --selinux-relabel
  virt-sparsify --compress --check-tmpdir=continue --tmp $IMAGES_DIR \
    $IMAGES_DIR/centos7-$DATE \
    $IMAGES_DIR/centos7-$DATE.qcow2
  rm -rf $IMAGES_DIR/centos7-$DATE
  curl --header "AccessKey: $BCDN_ACCESSKEY" --upload-file \
    $IMAGES_DIR/centos7-$DATE.qcow2 \
    https://storage.bunnycdn.com/blob/centos7-$DATE.qcow2
}

function fedora29 {
  curl -Lo $IMAGES_DIR/fedora29-$DATE $CLOUDIMAGE_FEDORA29
  virt-customize -v \
    -a $IMAGES_DIR/fedora29-$DATE \
    --run-command "echo 'fastestmirror=true' >> /etc/dnf/dnf.conf" \
    --run-command "dnf -y update" \
    --run-command "dnf -y install nano" \
    --run-command "dnf clean all" \
    --selinux-relabel
  virt-sparsify --compress --check-tmpdir=continue --tmp $IMAGES_DIR \
    $IMAGES_DIR/fedora29-$DATE \
    $IMAGES_DIR/fedora29-$DATE.qcow2
  rm -rf $IMAGES_DIR/fedora29-$DATE
  curl --header "AccessKey: $BCDN_ACCESSKEY" --upload-file \
    $IMAGES_DIR/fedora29-$DATE.qcow2 \
    https://storage.bunnycdn.com/blob/fedora29-$DATE.qcow2
}

function rhel7 {
  curl -Lo $IMAGES_DIR/rhel7-$DATE $CLOUDIMAGE_RHEL7
  # Hide from runner logs
  set +x
  virt-customize -v \
    -a $IMAGES_DIR/rhel7-$DATE \
    --run-command "subscription-manager register --org=$RHEL_ORG --activationkey=$RHEL_ACTIVATIONKEY" \
    --run-command "yum -y update" \
    --run-command "yum -y install nano" \
    --run-command "subscription-manager unregister" \
    --run-command "yum clean all" \
    --selinux-relabel
  set -x
#   virt-sparsify --compress --check-tmpdir=continue --tmp $IMAGES_DIR \
#     $IMAGES_DIR/rhel7-$DATE \
#     $IMAGES_DIR/rhel7-$DATE.qcow2
  rm -rf $IMAGES_DIR/rhel7-$DATE
  curl --header "AccessKey: $BCDN_ACCESSKEY" --upload-file \
    $IMAGES_DIR/rhel7-$DATE \
    https://storage.bunnycdn.com/blob/rhel7-$DATE.qcow2
}

function rhel8 {
  echo test
}

for arg in "$@"
do
  shift
  case $arg in
    "--ubuntu1804" )
      ubuntu1804 "$1";;
    "--debian9" )
      debian9 "$1";;
    "--centos7" )
      centos7 "$1";;
    "--fedora29" )
      fedora29 "$1";;
    "--rhel7" )
      rhel7 "$1";;
  esac
done
